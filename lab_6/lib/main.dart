import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: 'TemanCovid',
    theme: ThemeData(
      // Define the default brightness and colors.
      brightness: Brightness.light,

      // Define the default font family.
      fontFamily: 'Comfortaa',

      // Define the default `TextTheme`. Use this to specify the default
      // text styling for headlines, titles, bodies of text, and more.
      textTheme: const TextTheme(
        headline6: TextStyle(fontSize: 26.0),
      ),
    ),
    home: Profile(),
  ));
}

class Profile extends StatelessWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('TemanCovid'),
          backgroundColor: Colors.lightGreen,
          actions: [
            Icon(
              Icons.list,
            ),
          ],
        ),
        body: Container(
            padding: const EdgeInsets.all(30.0),
            child: Column(
              children: [
                Text(
                  "Brandon's Profile",
                  style: Theme.of(context).textTheme.headline6,
                ),
                Container(
                    margin: const EdgeInsets.only(top: 15, bottom: 15),
                    padding: const EdgeInsets.all(30.0),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        colors: <Color>[
                          Colors.white,
                          Colors.white,
                          Colors.white,
                          Colors.white,
                          Colors.lightGreenAccent,
                        ],
                      ),
                      borderRadius: BorderRadius.circular(10),
                      border: Border(
                        top: BorderSide(width: 0.1),
                        left: BorderSide(width: 0.1),
                        right: BorderSide(width: 0.1),
                        bottom: BorderSide(width: 0.1),
                      ),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.account_circle,
                          size: 200,
                        ),
                        Text(
                          "Blaxe",
                          style: TextStyle(fontSize: 27),
                        ),
                        Text(
                          "brandon@mail.com",
                          style: TextStyle(fontSize: 17, color: Colors.grey),
                        ),
                        Container(
                            padding: EdgeInsets.only(top: 25),
                            child: Text("Member since 12/12/1212",
                                style: TextStyle(fontWeight: FontWeight.bold))),
                      ],
                    )),
                Center(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Colors.lightGreen,
                    ),
                    child: const Text('Edit Profile'),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const EditProfile()),
                      );
                    },
                  ),
                ),
              ],
            )));
  }
}

class EditProfile extends StatelessWidget {
  const EditProfile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Edit Profile"),
          backgroundColor: Colors.lightGreen,
          actions: [
            Icon(
              Icons.list,
            ),
          ],
        ),
        body: Container(
            padding: const EdgeInsets.all(30.0),
            child: ListView(
              children: [
                Text(
                  "Edit Your Profile",
                  style: Theme.of(context).textTheme.headline6,
                ),
                Text(""),
                TextFormField(
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Username',
                  ),
                ),
                TextFormField(
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Email',
                  ),
                ),
                TextFormField(
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'First Name',
                  ),
                ),
                TextFormField(
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Last Name',
                  ),
                ),
                Text(""),
                Container(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Colors.lightGreen,
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: const Text('Save Changes'),
                  ),
                ),
              ],
            )));
  }
}
