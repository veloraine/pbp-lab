from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import FriendForm
from lab_1.models import Friend

# login_required guide by https://docs.djangoproject.com/en/3.2/topics/auth/default/
@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all()
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def addfriend(request):
    # Code copied from https://www.geeksforgeeks.org/django-modelform-create-form-from-models/
    context ={}
  
    # create object of form
    form = FriendForm(request.POST or None, request.FILES or None)
      
    # check if form data is valid
    if form.is_valid():
        # save the form data to model
        form.save()
        return redirect('/lab-3')
  
    context['form']= form
    return render(request, "lab3_form.html", context)