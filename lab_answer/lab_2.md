# Jawaban Lab 2

# Apakah perbedaan antara JSON dan XML?

## JSON:
1. Berdasarkan bahasa JavaScript
2. Merepresentasikan data dengan model seperti dictionary pada Python
3. Dapat diakses siapa saja (tidak secured)
4. Lebih mudah dibaca dibandingkan XML
5. Tampilan lebih simple dibanding XML

## XML
1. Berasal dari SGML (Standard Generalized Markup Language)
2. Merepresentasikan data dengan tag (karena merupakan markup language)
3. Lebih secured dibandingkan JSON
4. Lebih susah dibaca dibandingkan JSON
5. Lebih powerful dibandingkan dengan JSON

# Apakah perbedaan antara HTML dan XML?

## HTML
1. HTML lebih digunakan untuk penyajian data
2. HTML statis
3. Case sensitive
4. Closing tag tidak selalu penting
5. Jumlah tag yang tersedia terbatas

## XML
1. XML lebih digunakan dalam pengiriman (transfer) data
2. XML dinamis
3. Tidak case sensitive
4. Closing tag pada XML penting
5. Tag dalam XML dapat dikembangkan

## Sumber
1. https://www.geeksforgeeks.org/html-vs-xml/
2. https://www.geeksforgeeks.org/difference-between-json-and-xml/